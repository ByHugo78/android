import android.os.Bundle
import android.os.PersistableBundle
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.pruebas.R
import com.example.pruebas.ui.theme.PruebasTheme
import kotlin.random.Random

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PruebasTheme {
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    Column {
                        Huertos()
                    }
                }

            }

        }
    }

}


@Composable
fun Huertos() {
    var huerto_vacio by remember {
        mutableStateOf(R.drawable.huerto_vacio)
    }
    var huerto_planta by remember {
        mutableStateOf(R.drawable.huerto_plantas)
    }
    var huerto_fruto by remember {
        mutableStateOf(R.drawable.huerto_frutas)
    }
    var contador by remember {
        mutableStateOf(1)
    }
    var numero by remember {
        mutableStateOf(0)
    }
    Column() {
        if (contador == 1) {
            Image(painter = painterResource(id = huerto_vacio), contentDescription = "huerto")
        }
        if (contador == 2) {
            Image(painter = painterResource(id = huerto_planta), contentDescription = "huerto")
        }
        if (contador == 3) {
            Image(painter = painterResource(id = huerto_fruto), contentDescription = "huerto")
        }
        Button(
            onClick = {
            for (x in 1 ..1) {
                contador++
                if (contador == 1) {
                    huerto_vacio = R.drawable.huerto_vacio
                } else if (contador == 2) {
                    huerto_planta = R.drawable.huerto_plantas
                } else if (contador == 3) {
                    huerto_fruto = R.drawable.huerto_frutas
                    numero += (1..5).random()
                }else if(contador==4){
                    contador=1
                }
            }
        } ) {
            Text(text = "Pulsa Aqui")
        }
        Text("Valor: " + numero.toString())
    }
}

@Preview(showBackground = true)
@Composable
fun imprimir(){
    PruebasTheme {
        Huertos()
    }
}
